import {Component} from '@angular/core';
import {Store} from '@app/shared/models/store.model';
import {StoreService} from '@app/shared/services/store.service';

@Component({
  selector: 'app-store',
  templateUrl: 'store.component.html',
  styleUrls: ['store.component.scss']
})
export class StoreComponent {
  public stores: Store[] = [];

  constructor(private storeService: StoreService) {
  }

  // implement OnInit's `ngOnInit` method
  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit() { this.getStores(); }

  private getStores(): void {
    this.storeService.getStores().then(foundStores => this.stores = foundStores);
  }

}
