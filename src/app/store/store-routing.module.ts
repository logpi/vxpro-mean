import {RouterModule, Routes} from '@angular/router';
import {StoreComponent} from './store.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: StoreComponent
      }
    ],
  },
];

export const StoreRoutingModule = RouterModule.forChild(routes);
