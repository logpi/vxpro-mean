import {NgModule} from '@angular/core';
import {ModelModule} from '../shared/models/model.module';
import {StoreComponent} from './store.component';
import {StoreRoutingModule} from './store-routing.module';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [ModelModule, StoreRoutingModule, CommonModule],
  declarations: [StoreComponent],
  exports: [StoreComponent]
})
export class StoreModule {
}
