import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Store} from '@app/shared/models/store.model';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class StoreService {

  constructor(private http: HttpClient) {
  }

  private apiBaseUrl = `${environment.apiEndpoint}`;

  public getStores(): Promise<Store[]> {
    const url = `${this.apiBaseUrl}/stores`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => response as Store[])
      .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error('Something went wrong', error);
    return Promise.reject(error.message || error);
  }

}
