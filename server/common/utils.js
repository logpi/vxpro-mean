const {paging_limit} = require('../config/config')
const getPagingSize = function (req) {

  let {limit, index} = req.query;

  if (limit > paging_limit) limit = paging_limit

  return {
    skip: (limit * index) - limit,
    limit: limit
  }
}

module.exports = {
  getPagingSize
}
