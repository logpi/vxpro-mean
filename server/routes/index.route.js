const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const storeRoutes = require('./store.route');

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get('/health-check', (req, res) =>
  res.send('OK')
);

router.use('/auth', authRoutes);

router.use('/users', userRoutes);

router.use('/stores', storeRoutes);

module.exports = router;
