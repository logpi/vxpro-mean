const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler');
const storeCtrl = require('../controllers/store.controller');
const omitEmpty = require('omit-empty');

const router = express.Router();
module.exports = router;

router.route('/')
  .get(asyncHandler(getStores))
  .post(asyncHandler(insertStore));


async function getStores(req, res, next) {
  let query = utils.getPagingSize(req)
  let stores = await storeCtrl.getStores(query);
  res.json(omitEmpty(stores))
}

async function insertStore(req, res, next) {
  let store = await storeCtrl.createStore(req.body);
  res.json(omitEmpty(store));
}
