const express = require('express');
const passport = require('passport');
const asyncHandler = require('express-async-handler');
const userCtrl = require('../controllers/user.controller');

const router = express.Router();
module.exports = router;

// Verify if user is authenticated
router.use(passport.authenticate('jwt', {session: false}))

router.route('/')
  .post(asyncHandler(create));

router.route('/:userId')
  .get(asyncHandler(getUser))
  .delete(asyncHandler(deleteUser))
  .put(asyncHandler(updateUser))


async function create(req, res) {
  let user = await userCtrl.create(req.body);
  res.json(user);
}

async function getUser(req, res, next) {
  const {userId} = req.params;

  let user = await userCtrl.get(req);
  res.json(user)
}

async function deleteUser(req, res, next) {
  res.send('User is deleted');
}

async function updateUser(req, res, next) {
  res.send('User is updated');
}
