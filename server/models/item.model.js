const mongoose = require('mongoose')

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: String,
  // 1 item can have many categories
  categoryId: Array[String],
  price: {
    type: Number
  },
  updated: {
    type: Date,
    default: Date.now
  },
  updatedBy: String,
  createdBy: String
})


module.exports = mongoose.model('Item', itemSchema)
