const mongoose = require('mongoose')

const employeeSchema = new mongoose.Schema({
  name: String,
  position: String,
  description: String,
  phone: String,
  address: String,
  avatar: String
});

module.exports = mongoose.model('Employee', employeeSchema)
