const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  index: Number,
  description: String,
  isActive: {
    type: Boolean,
    default: false
  },
  updated: {
    type: Date,
    default: Date.now
  },
  // Parent id with sub category
  parentIdd: String
})


module.exports = mongoose.model('Category', categorySchema)
