const mongoose = require('mongoose')

const storeSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  address: {
    type: String
  },
  facebook: {
    type: String
  },
  hotline: {
    type: String
  },
  manager_name: {
    type: String
  },
  manager_phone: {
    type: String
  }

})


module.exports = mongoose.model('Store', storeSchema)
