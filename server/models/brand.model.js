const mongoose = require('mongoose')


const brandSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  logo: {
    type: String
  },
  description: {
    type: String
  },
  website: {
    type: String
  },
  facebook: {
    type: String
  },
  hotline: {
    type: String
  },
  stores: {
    type: Array[String]
  }
})


module.exports = mongoose.model('Brand', brandSchema)
