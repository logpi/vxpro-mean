const mongoose = require('mongoose')

const menuSchema = new mongoose.Schema({
  name: {
    type: String
  },
  items: {
    type: Array[String]
  },
  brand_id: {
    type: String
  }
});


