const Joi = require('joi');
const Store = require('../models/store.model');

// Define Joi Schema to verify
const storeSchema = Joi.object({
  name: Joi.string().required(),
});

module.exports = {
  getStores,
  createStore
}

// ======== Functions =========== //

async function getStores(query) {
  return await Store.find().limit(query.limit).skip(query.skip);
}

async function createStore(store) {
  // Validate store with joi-constraints
  store = await Joi.validate(store, storeSchema, {abortEarly: false});
  return await new Store(store).save();
}
